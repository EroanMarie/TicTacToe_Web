function setup() {
    //Mettre du code ici
    /*
    Initialisation de la partie fonctionelle du jeu
    */
    cases = [];
    for (i = 0; i < 3; i++) {
        cases[i] = [];
    }
    tour = 0;

    //Initialisation de la partie visuelle du jeu

    h = 600;
    w = 600;
    createCanvas(w, h);
    fill(64);
    strokeWeight(3);
    for (i = 0; i < 3; i++) {
        for (k = 0; k < 3; k++) {
            rect(i * w / 3, k * h / 3, w / 3, h / 3);
            cases[i][k] = "vide";
        }
    }
}

function draw() {
    //Mettre du code ici aussi
    if (mouseIsPressed) {
        check_position(mouseX, mouseY);
    }

}

function check_position(X, Y) {
    for (i = 0; i < 3; i++) {
        for (k = 0; k < 3; k++) {
            if (collidePointRect(X, Y, i * w / 3, k * h / 3, w / 3, h / 3)) {
                if (cases[i][k] == "vide") {
                    if (tour % 2 == 0) {
                        line(i * w / 3, k * h / 3, (i + 1) * w / 3, (k + 1) * h / 3);
                        line((1 + i) * w / 3, k * h / 3, i * w / 3, (k + 1) * h / 3);
                        cases[i][k] = "Joueur 1";

                    } else {
                        ellipse((i * w / 3) + (w / 6), (k * h / 3) + (h / 6), w / 3, h / 3);
                        cases[i][k] = "Joueur 2";
                    }
                    tour++;
                    //verification
                    //Joueur 1
                    //Vertical
                    if (cases[i][k] == "Joueur 1" && cases[i][0] == cases[i][1] && cases[i][1] == cases[i][2]) {
                        document.body.innerHTML = ("Joueur 1 gagne");
                    }
                    //Horizontal
                    if (cases[i][k] == "Joueur 1" && cases[0][k] == cases[1][k] && cases[1][k] == cases[2][k]) {
                        document.body.innerHTML = ("Joueur 1 gagne");
                    }
                    //Diagonales
                    if (cases[1][1] == "Joueur 1" && cases[0][2] == cases[1][1] && cases[1][1] == cases[2][0]) {
                        document.body.innerHTML = ("Joueur 1 gagne");
                    }
                    if (cases[1][1] == "Joueur 1" && cases[0][0] == cases[1][1] && cases[1][1] == cases[2][2]) {
                        document.body.innerHTML = ("Joueur 1 gagne");
                    }
                    //Joueur 2
                    //Vertical
                    if (cases[i][k] == "Joueur 2" && cases[i][0] == cases[i][1] && cases[i][1] == cases[i][2]) {
                        document.body.innerHTML = ("Joueur 2 gagne");
                    }
                    //Horizontal
                    if (cases[i][k] == "Joueur 2" && cases[0][k] == cases[1][k] && cases[1][k] == cases[2][k]) {
                        document.body.innerHTML = ("Joueur 2 gagne");
                    }
                    //Diagonales
                    if (cases[1][1] == "Joueur 2" && cases[0][2] == cases[1][1] && cases[1][1] == cases[2][0]) {
                        document.body.innerHTML = ("Joueur 2 gagne");
                    }
                    if (cases[1][1] == "Joueur 2" && cases[0][0] == cases[1][1] && cases[1][1] == cases[2][2]) {
                        document.body.innerHTML = ("Joueur 2 gagne");
                    }
                }
            }
        }
    }
}